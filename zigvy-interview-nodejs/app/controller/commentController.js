// Khai báo mongoose
const mongoose = require('mongoose');

// Import Model
const commentModel = require('../model/commentModel');


//CREATE A COMMENT
const createComment = (request, response) => {
    //B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!bodyRequest.name) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Name Is required"
        })
    }

    if (!bodyRequest.email) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Email Is required"
        })
    }

    if (!bodyRequest.body) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Body Is required"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let createComment = {
        _id: mongoose.Types.ObjectId(),
        postId: bodyRequest.postId,
        name: bodyRequest.name,
        email: bodyRequest.email,
        body: bodyRequest.body
    }
    commentModel.create(createComment, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Create Comment successfully",
                data: data
            })
        }
    })
}

// GET ALL COMMENT
const getAllComment = (request, response) => {
    //B1:Thu thập dữ liệu
    let postId = request.query.postId;

    let condition = {};
    if (postId) {
        condition.postId = postId;
    }
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    commentModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get All Comment successfully",
                data: data
            })
        }
    })
}


// GET COMMENT BY ID
const getCommentById = (request, response) => {
    //B1: Thu thập dữ liệu
    let commentId = request.params.commentId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Comment ID is not a valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    commentModel.findById(commentId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get Comment By ID successfully",
                data: data
            })
        }
    })
}


// UPDATE COMMENT BY ID
const updateCommentById = (request, response) => {
    //B1: thu thập dữ liệu
    let commentId = request.params.commentId;

    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Comment ID is not a valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let updateComment = {
        postId: bodyRequest.postId,
        name: bodyRequest.name,
        email: bodyRequest.email,
        body: bodyRequest.body
    }
    commentModel.findByIdAndUpdate(commentId, updateComment, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success Update Comment successfully",
                data: data
            })
        }
    })
}


//DELETE COMMENT BY ID
const deleteCommentById = (request, response) => {
    //B1: thu thập dữ liệu
    let commentId = request.params.commentId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Comment ID is not a valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    commentModel.findByIdAndDelete(commentId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete Comment By ID successfully",
            })
        }
    })
}


//GET COMMENTS OF POST
const getCommentsOfPost = (request, response) => {
    //B1:Thu thập dữ liệu
    let postId = request.params.postId;

    let condition = {};
    if (postId) {
        condition.postId = postId;
    }
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    commentModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get All Comment successfully",
                data: data
            })
        }
    })
}


//EXPORT
module.exports = { createComment, getAllComment, getCommentById, updateCommentById, deleteCommentById, getCommentsOfPost };