// Import Model
const userModel = require('../model/userModel');

//Khai báo mongoose
const mongoose = require('mongoose');

//CREATE A USER
const createUser = (request, response) => {
    //B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!bodyRequest.name) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Name Is required",
        })
    }

    if (!bodyRequest.username) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User Name Is required",
        })
    }

    if (!bodyRequest.email) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Email Is required",
        })
    }

    if (!bodyRequest.address.street) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Address Street Is required",
        })
    }

    if (!bodyRequest.address.suite) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Address Suite Is required",
        })
    }

    if (!bodyRequest.address.city) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Address City Is required",
        })
    }

    if (!bodyRequest.address.zipcode) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Address Zipcode Is required",
        })
    }

    if (!bodyRequest.address.geo.lat) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Address Geo Lat Is required",
        })
    }

    if (!bodyRequest.address.geo.lng) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Address Geo Lng Is required",
        })
    }

    if (!bodyRequest.phone) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Phone Is required",
        })
    }

    if (!bodyRequest.website) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Web Is required",
        })
    }

    if (!bodyRequest.company.name) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Company Name Is required",
        })
    }

    if (!bodyRequest.company.catchPhrase) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Company CatchPhrase Is required",
        })
    }

    if (!bodyRequest.company.bs) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Company BS Is required",
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let createUser = {
        _id: mongoose.Types.ObjectId(),
        name: bodyRequest.name,
        username: bodyRequest.username,
        email: bodyRequest.email,
        address: bodyRequest.address,
        phone: bodyRequest.phone,
        website: bodyRequest.website,
        company: bodyRequest.company
    }

    userModel.create(createUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Create user success",
                data: data
            })
        }
    })

}


//GET ALL USER
const getAllUser = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all user success",
                data: data
            })
        }
    })
}


//GET A USER BY ID
const getUserById = (request, response) => {
    //B1: Thu thập dữ liệu
    let userId = request.params.userId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not a valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    userModel.findById(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: get User By ID successfully",
                data: data
            })
        }
    })
}


// UPDATE USER BY ID
const updateUserByID = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not a valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updateUser = {
        name: bodyRequest.name,
        username: bodyRequest.username,
        email: bodyRequest.email,
        address: bodyRequest.address,
        phone: bodyRequest.phone,
        website: bodyRequest.website,
        company: bodyRequest.company
    }
    userModel.findByIdAndUpdate(userId, updateUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update User By ID successfully",
                data: data
            })
        }
    })

}


//DELETE USER
const deleteUserById = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not a valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete User By ID successfully"
            })
        }
    })
}


// EXPORT
module.exports = { createUser, getAllUser, getUserById, updateUserByID, deleteUserById }