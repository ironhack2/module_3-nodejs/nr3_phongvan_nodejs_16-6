// Import Model
const albumModel = require('../model/albumModel');

//  Khai báo mongoose
const mongoose = require('mongoose');

//CREATE A ALBUM
const createAlbum = (request, response) => {
    //B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: validate dữ liệu

    if (!bodyRequest.userId) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Title Is required",
        })
    }

    if (!bodyRequest.title) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Title Is required",
        })
    }


    //B3: thao tác với cơ sở dữ liệu
    let createAlbum = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyRequest.userId,
        title: bodyRequest.title,
    }
    albumModel.create(createAlbum, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Create ALBUM success",
                data: data
            })
        }
    })
}


//GET ALL ALBUM
const getAllAlbum = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.query.userId;

    let condition = {};

    if (userId) {
        condition.userId = userId;
    }
    //B2: validate dữ liệu

    //B3: thao tác với cơ sở dữ liệu
    albumModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get ALL ALBUM",
                data: data
            })
        }
    })
}

//GET ALBUM BY ID
const getAlbumById = (request, response) => {
    //B1: thu thập dữ liệu
    let albumId = request.params.albumId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Album ID is not a valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    albumModel.findById(albumId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get Album by Id successfully",
                data: data
            })
        }
    })
}

//UPDATE Album
const updateAlbumByID = (request, response) => {
    //B1: thu thập dữ liệu
    let albumId = request.params.albumId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Album ID is not a valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updateAlbum = {
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        body: bodyRequest.body
    }
    albumModel.findByIdAndUpdate(albumId, updateAlbum, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success Update Album successfully",
                data: data
            })
        }
    })
}


// DELETE Album
const deleteAlbumById = (request, response) => {
    //B1: thu thập dữ liệu
    let albumId = request.params.albumId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Album ID is not a valid"
        })
    }

    // B3: thao tác với cơ sở dữ liệu
    albumModel.findByIdAndDelete(albumId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete Album successfully"
            })
        }
    })

}


//GET AlbumS OF USER
const getAlbumsOfUser = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    let condition = {};

    if (userId) {
        condition.userId = userId;
    }

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    albumModel.find(condition, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get All Album Of User Id: " + userId,
                data: data
            })
        }
    })
}


// EXPORT
module.exports = { createAlbum, getAllAlbum, getAlbumById, updateAlbumByID, deleteAlbumById, getAlbumsOfUser }