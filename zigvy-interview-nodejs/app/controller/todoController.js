//Khai báo mongoose
const mongoose = require('mongoose');

//Import Model
const todoModel = require('../model/todoModel');

//CREATE A TODO
const createTodo = (request, response) => {
    //B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!bodyRequest.title) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Title Is required"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let createTodo = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        completed: bodyRequest.completed
    }
    todoModel.create(createTodo, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Create Todo successfully",
                data: data
            })
        }
    })
}


// GET ALL TODO
const getAllTodo = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.query.userId;

    let condition = {};
    if (userId) {
        condition.userId = userId;
    }
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    todoModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get All Todo successfully",
                data: data
            })
        }
    })
}


//GET TODO BY ID
const getTodoById = (request, response) => {
    //B1: thu thập dữ liệu
    let todoId = request.params.todoId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Todo ID is not a valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    todoModel.findById(todoId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get Todo By Id successfully",
                data: data
            })
        }
    })

}

// UPDATE TODO BY ID
const updateTodoById = (request, response) => {
    //B1: thu thập dữ liệu
    let todoId = request.params.todoId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Todo ID is not a valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    let updateTodo = {
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        completed: bodyRequest.completed
    }
    todoModel.findByIdAndUpdate(todoId, updateTodo, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update Todo successfully",
                data: data
            })
        }
    })
}

// DELETE TODO BY ID
const deleteTodoById = (request, response) => {
    //B1: thu thập dữ liệu
    let todoId = request.params.todoId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Todo ID is not a valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    todoModel.findByIdAndDelete(todoId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: `Success: Delete Todo By ID successfully`
            })
        }
    })
}


// GET  TODO  OF USER
const getTodosOfUser = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    let condition = {};
    if (userId) {
        condition.userId = userId;
    }
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    todoModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get All Todo successfully",
                data: data
            })
        }
    })
}


//EXPORT
module.exports = { createTodo, getAllTodo, getTodoById, updateTodoById, deleteTodoById, getTodosOfUser }