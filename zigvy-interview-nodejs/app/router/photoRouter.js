//Khai báo thư viện express
const express = require('express');

//Import Middleware
const { photoMiddleware } = require('../middleware/photoMiddleware');


//Import Controller
const { createPhoto, getAllPhoto, getPhotoById, updatePhotoByID, deletePhotoById, getPhotosOfUser } = require('../controller/photoController');

//Tạo router
const photoRouter = express.Router();

//Sử dụng middleware
photoRouter.use(photoMiddleware);


//Khai báo API

//CREATE photo
photoRouter.post('/photos', createPhoto);


// GET ALL photo
photoRouter.get('/photos', getAllPhoto);


//GET photo BY ID
photoRouter.get('/photos/:photoId', getPhotoById);


//UPDATE photo
photoRouter.put('/photos/:photoId', updatePhotoByID);


//DELETE photo
photoRouter.delete('/photos/:photoId', deletePhotoById);


//GET photoS OF USER
photoRouter.get('/albums/:albumId/photos', getPhotosOfUser);

module.exports = photoRouter;