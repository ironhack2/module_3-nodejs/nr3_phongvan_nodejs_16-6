//Khai báo thư viện express
const express = require('express');

//Import Middleware
const { commentMiddleware } = require('../middleware/commentMiddleware');


//Import Controller
const { createComment, getAllComment, getCommentById, updateCommentById, deleteCommentById, getCommentsOfPost } = require('../controller/commentController');

//Tạo router
const commentRouter = express.Router();

//Sử dụng middleware
commentRouter.use(commentMiddleware);


//Khai báo API

//CREATE USER
commentRouter.post('/comments', createComment);

// GET ALL USER
commentRouter.get('/comments', getAllComment);


//GET USER BY ID
commentRouter.get('/comments/:commentId', getCommentById);


//UPDATE USER
commentRouter.put('/comments/:commentId', updateCommentById);


//DELETE USER
commentRouter.delete('/comments/:commentId', deleteCommentById);



//GET COMMENTS OF POST
commentRouter.get('/posts/:postId/comments', getCommentsOfPost);



//EXPORT
module.exports = commentRouter;