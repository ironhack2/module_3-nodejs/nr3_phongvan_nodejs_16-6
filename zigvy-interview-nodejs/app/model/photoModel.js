//Khai báo mongoose
const mongoose = require('mongoose');

//Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// Khởi tạo thuộc tính Schema được yêu cầu
const photoSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    albumId: {
        type: mongoose.Types.ObjectId,
        ref: "album"
    },
    title: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    thumbnailUrl: {
        type: String,
        required: true
    }
})

// EXPORT
module.exports = mongoose.model("photo", photoSchema);