// Khai báo mongoose
const mongoose = require('mongoose');

// Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;


// Khởi tạo Schema với các thuộc tính được yêu cầu

const geoSchema = new Schema({
    lat: {
        type: String,
        required: true
    },
    lng: {
        type: String,
        required: true
    }
})



const addressSchema = new Schema({
    street: {
        type: String,
        required: true
    },
    suite: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    zipcode: {
        type: String,
        required: true
    },
    geo: geoSchema
})

const companySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    catchPhrase: {
        type: String,
        required: true
    },
    bs: {
        type: String,
        required: true
    },
})

// Khởi tạo Schema với các thuộc tính được yêu cầu
const userSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true
    },
    address: addressSchema,
    phone: {
        type: String,
        required: true
    },
    website: {
        type: String,
        required: true
    },
    company: companySchema
})

// EXPORT
module.exports = mongoose.model("user", userSchema);