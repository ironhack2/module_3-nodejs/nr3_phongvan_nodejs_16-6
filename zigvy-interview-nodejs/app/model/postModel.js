//Khai báo mongoose
const mongoose = require('mongoose');

//Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// Khởi tạo thuộc tính Schema được yêu cầu
const postSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    userId: {
        type: mongoose.Types.ObjectId,
        ref: "user"
    },
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    }
})

// EXPORT
module.exports = mongoose.model("post", postSchema);