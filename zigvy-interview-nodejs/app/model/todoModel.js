//Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// Khởi tạo Schema với các thuộc tính được yêu cầu
const todoSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    userId: {
        type: mongoose.Types.ObjectId
    },
    title: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean,
        default: false
    }
})


//EXPORT
module.exports = mongoose.model("todo", todoSchema);