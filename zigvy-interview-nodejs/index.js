//Khai báo thư viện express
const express = require('express');

// Khai báo thư viện mongoose
const mongoose = require('mongoose');


// Import Router
const userRouter = require('./app/router/userRouter');
const postRouter = require('./app/router/postRouter');
const commentRouter = require('./app/router/commentRouter');
const albumRouter = require('./app/router/albumRouter');
const photoRouter = require('./app/router/photoRouter');
const todoRouter = require('./app/router/todoRouter');



// Khai báo app nodeJS
const app = new express();

// Khai báo Middleware Json
app.use(express.json());

//Khai báo middleware đọc dư liệu UTF-8
app.use(express.urlencoded({
    extended: true,
}))

//Khai báo cổng nodeJS
const port = 8000;

mongoose.connect("mongodb://localhost:27017/CRUD_Zigvy", (error) => {
    if (error) {
        throw error;
    }

    console.log(`Connect MongoDB successfully`)
})

//  Sử dụng router
app.use('/', userRouter);
app.use('/', postRouter);
app.use('/', commentRouter);
app.use('/', albumRouter);
app.use('/', photoRouter);
app.use('/', todoRouter);

//  Chạy trên cổng nodejs
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})